package coinbase.api

import coinbase.api.http.data.Currency
import coinbase.api.http.data.Product
import coinbase.api.infra.moshi
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Types
import org.http4k.client.OkHttp
import org.http4k.core.Method.GET
import org.http4k.core.Request

class CoinbasePublicHttpClient(httpUrl: String) {

    private val GET_PRODUCTS = Request(GET, "$httpUrl/products")
    private val GET_CURRENCIES = Request(GET, "$httpUrl/currencies")

    private val productListMapper: JsonAdapter<List<Product>> = moshi.adapter(Types.newParameterizedType(List::class.java, Product::class.java))
    private val currenciesMapper: JsonAdapter<List<Currency>> = moshi.adapter(Types.newParameterizedType(List::class.java, Currency::class.java))

    private val http by lazy {
        OkHttp()
    }

    fun getProducts(): List<Product> = http(GET_PRODUCTS).let { productListMapper.fromJson(it.bodyString())!! }

    fun getCurrencies(): List<Currency> = http(GET_CURRENCIES).let { currenciesMapper.fromJson(it.bodyString())!! }
}
