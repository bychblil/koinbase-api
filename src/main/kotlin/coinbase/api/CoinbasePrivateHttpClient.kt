package coinbase.api

import coinbase.api.http.CoinbaseMessageSigner
import coinbase.api.http.data.Account
import coinbase.api.http.data.AccountEntry
import coinbase.api.http.data.CoinbaseFill
import coinbase.api.http.data.Fee
import coinbase.api.http.data.OrderRequest
import coinbase.api.http.data.OrderResponse
import coinbase.api.infra.moshi
import com.google.common.collect.EvictingQueue
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Types
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.http4k.client.OkHttp
import org.http4k.core.Headers
import org.http4k.core.Method.GET
import org.http4k.core.Method.POST
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status.Companion.NOT_FOUND
import org.slf4j.LoggerFactory
import java.time.Instant

class CoinbasePrivateHttpClient(httpUrl: String, key: String, secret: String, passphrase: String) {

    private val GET_ACCOUNT = "$httpUrl/accounts"
    private val GET_FEES = "$httpUrl/fees"
    private val ORDERS = "$httpUrl/orders"
    private val GET_FILLS = "$httpUrl/fills"

    private val httpClient = HttpClient(key, passphrase, secret)

    private val accountListMapper: JsonAdapter<List<Account>> = moshi.adapter(Types.newParameterizedType(List::class.java, Account::class.java))
    private val accountEntryListMapper: JsonAdapter<List<AccountEntry>> = moshi.adapter(Types.newParameterizedType(List::class.java, AccountEntry::class.java))
    private val fillListMapper: JsonAdapter<List<CoinbaseFill>> = moshi.adapter(Types.newParameterizedType(List::class.java, CoinbaseFill::class.java))

    fun getAccounts(): List<Account> = httpClient.sendRequest(Request(GET, GET_ACCOUNT)).let {
        accountListMapper.fromJson(it.bodyString()) ?: emptyList()
    }

    fun getFees(): Fee = httpClient.sendRequest(Request(GET, GET_FEES)).let {
        moshi.adapter(Fee::class.java).fromJson(it.bodyString())!!
    }

    fun postOrder(request: OrderRequest): OrderResponse =
        httpClient.sendRequest(
            Request(POST, ORDERS).body(moshi.adapter(OrderRequest::class.java).toJson(request))
        ).let { moshi.adapter(OrderResponse::class.java).fromJson(it.bodyString())!! }

    fun getOrder(orderId: String): OrderResponse? =
        httpClient.sendRequest(Request(GET, "$ORDERS/$orderId"))
            .let {
                if (it.status == NOT_FOUND) {
                    null
                } else {
                    moshi.adapter(OrderResponse::class.java).fromJson(it.bodyString())!!
                }
            }

    fun getFillsByOrderId(orderId: String) =
        httpClient.sendRequest(Request(GET, GET_FILLS).query("order_id", orderId))
            .let { fillListMapper.fromJson(it.bodyString()) ?: emptyList() }

    fun getFillsByProductIdFromTrade(productId: String, tradeId: String): List<CoinbaseFill> {
        /* recursive */
        httpClient.sendRequest(
            Request(GET, GET_FILLS)
                .query("product_id", productId)
                .query("before", tradeId)
        )
            .let { response ->
                val fills = fillListMapper.fromJson(response.bodyString()) ?: emptyList()
                val lastTradeId = response.header(PAGINATION_BEFORE_CURSOR_HEADER)
                if (fills.size >= 100 && lastTradeId != null) {
                    return fills + getFillsByProductIdFromTrade(productId, lastTradeId)
                }
                return fills
            }
    }

    fun getFullAccountLedger(
        accId: String,
        afterCursor: String? = null,
        entries: List<AccountEntry> = listOf()
    ): List<AccountEntry> {
        val paginationUrlPart = afterCursor?.let { "?after=$afterCursor" } ?: ""
        return httpClient.sendRequest(Request(GET, "$GET_ACCOUNT/$accId/ledger$paginationUrlPart")).let {
            val responseEntries = accountEntryListMapper.fromJson(it.bodyString()) ?: emptyList()
            it.header(PAGINATION_AFTER_CURSOR_HEADER)?.let { cursorHeader ->
                getFullAccountLedger(accId, cursorHeader, entries + responseEntries)
            } ?: entries + responseEntries
        }
    }

    private class HttpClient(key: String, passphrase: String, secret: String) {
        /**
         * is checked for request per second limit. Size of the queue corresponds to per second limit.
         */
        private val timestampQueue = EvictingQueue.create<Long>(5).apply { add(0) }

        private val messageSigner = CoinbaseMessageSigner(secret)
        private val http by lazy { OkHttp() }

        private val headers: Headers = listOf(
            "Content-Type" to "application/json",
            "CB-ACCESS-KEY" to key,
            "CB-ACCESS-PASSPHRASE" to passphrase
        )

        /**
         * every http call should route through here
         */
        fun sendRequest(req: Request): Response {
            val secondsTimestamp = Instant.now().epochSecond.toString()
            val reqWithHeaders = req.headers(headers)
                .header("CB-ACCESS-TIMESTAMP", secondsTimestamp)
                .header("CB-ACCESS-SIGN", messageSigner.signMessage(req, secondsTimestamp))

            // TODO understand coroutines and reimplement with it - this blocks
            runBlocking {
                delayIfNeeded()
            }

            logReq(reqWithHeaders)
            return http(reqWithHeaders).also {
                if (!it.status.successful) {
                    log.error("HTTP ${it.status.code} ${it.bodyString()}")
                } else {
                    logResp(it)
                }
            }
        }

        private fun logReq(r: Request) = log.info("${r.method} ${r.uri} ${r.bodyString()}")

        private fun logResp(r: Response) = log.info("HTTP${r.status} ${r.bodyString()}")

        private suspend fun delayIfNeeded() {
            val timestampMillis = Instant.now().toEpochMilli()
            val millisDiff = timestampMillis - timestampQueue.peek()
            if (millisDiff < 1000) {
                delay(millisDiff)
            }
            timestampQueue.add(timestampMillis)
        }
    }

    companion object {
        private val log = LoggerFactory.getLogger(CoinbasePrivateHttpClient::class.java)
        private const val PAGINATION_AFTER_CURSOR_HEADER = "CB-AFTER"
        private const val PAGINATION_BEFORE_CURSOR_HEADER = "CB-BEFORE"
    }
}
