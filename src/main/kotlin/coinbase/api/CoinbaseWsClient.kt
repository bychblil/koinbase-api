package coinbase.api

import coinbase.api.infra.Metrics
import coinbase.api.infra.moshi
import coinbase.api.util.schedule
import coinbase.api.ws.Channel
import coinbase.api.ws.Channel.Error
import coinbase.api.ws.Channel.Heartbeat
import coinbase.api.ws.Channel.Subscriptions
import coinbase.api.ws.Channel.Ticker
import coinbase.api.ws.data.CoinbaseMessage
import coinbase.api.ws.data.SubscribeMessage
import coinbase.api.ws.handler.HeartbeatHandler
import coinbase.api.ws.handler.MessageHandler
import coinbase.api.ws.handler.SubscriptionHandler
import coinbase.api.ws.handler.TickerHandler
import kotlinx.coroutines.Job
import org.http4k.client.WebsocketClient
import org.http4k.core.Uri
import org.http4k.websocket.Websocket
import org.http4k.websocket.WsMessage
import org.slf4j.LoggerFactory

class CoinbaseWsClient(
    private val appName: String = "coinbase-client",
    private val webSocketUrl: String,
    private val channels: Set<Channel>,
    private val handlers: List<MessageHandler<*>>,
    private val scannedProducts: Set<String>
) {

    private val log = LoggerFactory.getLogger(this.javaClass)
    private val subscribeMessageAdapter = moshi.adapter(SubscribeMessage::class.java)

    private var messageCounter: Long = 0
    private var connectionCheck: Job? = null

    fun connect() {
        WebsocketClient.nonBlocking(uri = Uri.of(webSocketUrl)) { ws ->
            ws.onMessage {
                messageCounter++
                handleMessage(it.bodyString())
            }
            ws.onError { log.error("Websocket error", it) }
            ws.onClose {
                log.error("Websocket closed: {}. Reconnecting", it.description)
                connect()
            }
            subscribe(ws)
            scheduleConnectionCheck(ws)
        }
    }

    /*
     * Creates job that checks once per minute that WebSocket is connected and tries to reconnect if its not.
     * Connection is detected by number of incoming messages.
     */
    private fun scheduleConnectionCheck(ws: Websocket) {
        connectionCheck?.cancel() // cancel any previous job

        messageCounter = 1
        var lastCount: Long = 0

        connectionCheck = schedule(60_000) {
            val current = messageCounter
            if (lastCount >= current) {
                log.warn("No messages from websocket. Trying to reconnect")
                ws.close()
                connect()
            } else {
                lastCount = current
            }
        }
    }

    private fun handleMessage(payload: String) {
        log.trace(payload)
        val message: CoinbaseMessage = payload.toKotlinObject()
        Metrics.incWebSocketInTotal(appName, message.type)

        when (val ch = Channel.from(message.type)) {
            Ticker -> handlers.filterIsInstance<TickerHandler>().forEach { it.handleMessage(payload.toKotlinObject()) }
            Heartbeat -> handlers.filterIsInstance<HeartbeatHandler>().forEach { it.handleMessage(payload.toKotlinObject()) }
            Subscriptions -> handlers.filterIsInstance<SubscriptionHandler>().forEach { it.handleMessage(payload.toKotlinObject()) }
            Error -> log.error(payload)
            else -> throw IllegalArgumentException("channel ${ch.channelName} not implemented.")
        }
    }

    private fun subscribe(ws: Websocket) {
        log.info("Coinbase websocket subscribing")
        val payload = subscribeMessageAdapter.toJson(
            SubscribeMessage(
                scannedProducts,
                (channels + Heartbeat).map { it.channelName }
            )
        )
        log.debug("Payload {}", payload)
        ws.send(WsMessage(payload))
    }
}

inline fun <reified T : Any> String.toKotlinObject(): T =
    moshi.adapter(T::class.java).fromJson(this) ?: throw RuntimeException("Failed to parse $this to ${T::class.java}")
