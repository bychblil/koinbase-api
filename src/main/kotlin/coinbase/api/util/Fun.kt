package coinbase.api.util

import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import org.slf4j.Logger
import org.slf4j.LoggerFactory

val log: Logger = LoggerFactory.getLogger("coinbase.api.util")

fun schedule(delayMillis: Long, run: () -> Any): Job = GlobalScope.launch {
    while (isActive) {
        try {
            delay(delayMillis)
            run()
        } catch (t: Throwable) {
            if (t !is CancellationException) {
                log.error("Exception in scheduled function", t)
            }
        }
    }
}
