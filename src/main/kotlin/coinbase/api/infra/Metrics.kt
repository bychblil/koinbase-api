package coinbase.api.infra

import io.prometheus.client.Counter

object Metrics {

    private const val APP_LABEL = "app"
    private const val SP_LABEL = "provider"
    private const val SP_VALUE = "coinbase"

    private val incomingWebsocketMessagesTotal = Counter.build()
        .name("ws_messages_in_total")
        .labelNames(APP_LABEL, SP_LABEL, "channel")
        .help("Incoming websocket messages count")
        .register()

    fun incWebSocketInTotal(appName: String, channel: String): Unit =
        incomingWebsocketMessagesTotal.labels(appName, SP_VALUE, channel).inc()
}
