package coinbase.api.infra

import com.squareup.moshi.FromJson
import com.squareup.moshi.Moshi
import com.squareup.moshi.ToJson
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import java.math.BigDecimal
import java.time.Instant

object BigDecimalAdapter {
    @FromJson fun fromJson(string: String) = BigDecimal(string)
    @ToJson fun toJson(value: BigDecimal) = value.toString()
}

object InstantAdapter {
    @FromJson fun fromJson(string: String) = Instant.parse(string)
    @ToJson fun toJson(value: Instant) = value.toString()
}

val moshi: Moshi = Moshi.Builder()
    .add(BigDecimalAdapter)
    .add(InstantAdapter)
    .add(KotlinJsonAdapterFactory())
    .build()
