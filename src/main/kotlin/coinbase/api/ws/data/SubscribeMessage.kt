package coinbase.api.ws.data

data class SubscribeMessage(
    val product_ids: Set<String>,
    val channels: Collection<String>
) : CoinbaseMessage("subscribe")
