package coinbase.api.ws.data

import java.time.Instant

data class HeartbeatMessage(
    val sequence: Long,
    val last_trade_id: Long,
    val product_id: String,
    val time: Instant
) : CoinbaseMessage("heartbeat")
