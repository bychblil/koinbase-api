package coinbase.api.ws.data

import java.math.BigDecimal
import java.time.Instant

data class TickerMessage(
    val trade_id: Long?,
    val sequence: Long,
    val time: Instant,
    val product_id: String,
    val price: BigDecimal,
    val side: String?,
    val last_size: BigDecimal,
    val best_bid: BigDecimal,
    val open24h: BigDecimal?,
    val low24h: BigDecimal?,
    val high24h: BigDecimal?,
    val volume24h: BigDecimal?,
    val volume30d: BigDecimal?,
    val best_ask: BigDecimal
) : CoinbaseMessage("ticker")
