package coinbase.api.ws.data

data class ChannelData(
    val name: String,
    val product_ids: Set<String>
)
