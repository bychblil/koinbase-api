package coinbase.api.ws.data

data class SubscriptionsMessage(
    val channels: Set<ChannelData>
) : CoinbaseMessage("subscriptions")
