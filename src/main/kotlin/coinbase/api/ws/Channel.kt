package coinbase.api.ws

enum class Channel(val channelName: String) {
    Ticker("ticker"),
    Heartbeat("heartbeat"),
    Subscriptions("subscriptions"),
    Error("error");

    companion object {
        fun from(channelName: String) =
            values().find { it.channelName == channelName } ?: throw IllegalArgumentException("No channel with name $channelName")
    }
}
