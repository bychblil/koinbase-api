package coinbase.api.ws.handler

import coinbase.api.ws.data.CoinbaseMessage
import coinbase.api.ws.data.HeartbeatMessage
import coinbase.api.ws.data.SubscribeMessage
import coinbase.api.ws.data.TickerMessage

interface HeartbeatHandler : MessageHandler<HeartbeatMessage>
interface TickerHandler : MessageHandler<TickerMessage>
interface SubscriptionHandler : MessageHandler<SubscribeMessage>

interface MessageHandler<T : CoinbaseMessage> {
    fun handleMessage(message: T)
}
