package coinbase.api.ws.handler

import coinbase.api.ws.data.ChannelData
import coinbase.api.ws.data.SubscriptionsMessage

object SubscriptionsHandler : MessageHandler<SubscriptionsMessage> {

    val subscribedChannels: MutableSet<ChannelData> = mutableSetOf()

    override fun handleMessage(message: SubscriptionsMessage) {
        subscribedChannels.addAll(message.channels)
    }
}
