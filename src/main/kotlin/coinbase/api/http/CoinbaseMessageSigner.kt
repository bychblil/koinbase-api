package coinbase.api.http

import org.http4k.core.Request
import java.util.Base64
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec
import javax.management.RuntimeErrorException

class CoinbaseMessageSigner(secret: String) {

    private val decodedSecret = Base64.getDecoder().decode(secret)
    private val mac: Mac = Mac.getInstance("HmacSHA256")

    fun signMessage(req: Request, timestamp: String): String {
        val reqPath = req.uri.path + if (req.uri.query == "") "" else "?${req.uri.query}"
        val prehash = timestamp + req.method.name + reqPath + req.bodyString()
        return try {
            val keyspec = SecretKeySpec(decodedSecret, mac.algorithm)
            val sha256: Mac = mac.clone() as Mac
            sha256.init(keyspec)
            Base64.getEncoder().encodeToString(sha256.doFinal(prehash.toByteArray()))
        } catch (e: Exception) {
            e.printStackTrace()
            throw RuntimeErrorException(Error("Cannot set up authentication headers."))
        }
    }
}
