package coinbase.api.http.data

import java.math.BigDecimal
import java.time.Instant

data class CoinbaseFill(
    val trade_id: String,
    val product_id: String,
    val price: BigDecimal,
    val size: BigDecimal,
    val order_id: String,
    val created_at: Instant,
    val liquidity: String, // T or M
    val fee: BigDecimal,
    val settled: Boolean,
    val side: String
)
