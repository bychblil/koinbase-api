package coinbase.api.http.data

import java.math.BigDecimal

data class Product(
    val id: String,
    val base_currency: String,
    val quote_currency: String,
    val base_min_size: BigDecimal,
    val base_max_size: BigDecimal,
    val quote_increment: BigDecimal,
    val base_increment: BigDecimal
)
