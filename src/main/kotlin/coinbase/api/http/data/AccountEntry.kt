package coinbase.api.http.data

import java.math.BigDecimal
import java.time.Instant

data class AccountEntry(
    val id: String,
    val created_at: Instant,
    val amount: BigDecimal,
    val balance: BigDecimal,
    val type: String,
    val details: Details
) {
    data class Details(
        val order_id: String?,
        val trade_id: String?,
        val product_id: String?
    )
}
