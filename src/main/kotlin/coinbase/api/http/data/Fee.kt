package coinbase.api.http.data

import java.math.BigDecimal

data class Fee(
    val maker_fee_rate: BigDecimal,
    val taker_fee_rate: BigDecimal
)
