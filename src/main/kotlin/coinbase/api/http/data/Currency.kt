package coinbase.api.http.data

import java.math.BigDecimal

data class Currency(
    val id: String,
    val min_size: BigDecimal,
    val status: String,
    val max_precision: BigDecimal
)
