package coinbase.api.http.data

import java.math.BigDecimal

data class Account(
    val id: String,
    val currency: String,
    val balance: BigDecimal,
    val available: BigDecimal,
    val hold: BigDecimal,
    val profile_id: String
)
