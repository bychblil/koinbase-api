package coinbase.api.http.data

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.math.BigDecimal
import java.time.Instant

data class OrderResponse(
    val id: String,
    val client_oid: String? = null,
    val price: BigDecimal,
    val size: BigDecimal,
    val product_id: String,
    val side: String,
    val stp: String? = null, // self trade prevention
    val type: String? = null, // limit or market
    val time_in_force: String? = null,
    val post_only: Boolean? = null,
    val created_at: Instant,
    val fill_fees: BigDecimal,
    val filled_size: BigDecimal,
    val executed_value: BigDecimal,
    val status: String,
    val settled: Boolean
) {

    fun isCanceled() = status.equals("canceled", true)

    companion object {
        val logger: Logger = LoggerFactory.getLogger(OrderResponse::class.java)
    }
}
