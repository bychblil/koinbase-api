package coinbase.api.http.data

import java.math.BigDecimal
import java.util.UUID

data class OrderRequest(
    val size: BigDecimal,
    val price: BigDecimal,
    val side: String,
    val product_id: String,
    val client_oid: String = UUID.randomUUID().toString(),
    val type: String = "limit", // limit or market
    val time_in_force: String? = "GTT",
    val cancel_after: String? = "hour"
)
